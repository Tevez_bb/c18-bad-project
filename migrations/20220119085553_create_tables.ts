import { Knex } from "knex";

export async function up(knex: Knex) {
  await knex.schema.createTable("experience", (table) => {
    table.increments();
    table.string("name");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("user", (table) => {
    table.increments();
    table.string("email");
    table.string("name");
    table.string("password");
    table.integer("experience_id").unsigned();
    table.foreign("experience_id").references("experience.id");
    table.string("gender");
    table.string("age_group");
    table.string("icon");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("target_area", (table) => {
    table.increments();
    table.string("name");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("pose", (table) => {
    table.increments();
    table.string("name");
    table.string("image");
    table.integer("target_area_id").unsigned();
    table.foreign("target_area_id").references("target_area.id");
    table.integer("experience_id").unsigned();
    table.foreign("experience_id").references("experience.id");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("user_target_area", (table) => {
    table.increments();
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.integer("target_area_id").unsigned();
    table.foreign("target_area_id").references("target_area.id");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("bookmark", (table) => {
    table.increments();
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.integer("pose_id").unsigned();
    table.foreign("pose_id").references("pose.id");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("history", (table) => {
    table.increments();
    table.boolean("completed");
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.integer("pose_id").unsigned();
    table.foreign("pose_id").references("pose.id");
    table.integer("accuracy").unsigned();
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex) {
  await knex.schema.dropTableIfExists("history");
  await knex.schema.dropTableIfExists("bookmark");
  await knex.schema.dropTableIfExists("user_target_area");
  await knex.schema.dropTableIfExists("pose");
  await knex.schema.dropTableIfExists("target_area");
  await knex.schema.dropTableIfExists("user");
  await knex.schema.dropTableIfExists("experience");
}
