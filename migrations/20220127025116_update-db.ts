import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("pose")){
        await knex.schema.table("pose", (table)=>{
            table.integer("class_id");
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("pose")){
        await knex.schema.alterTable('pose',(table)=>{
            table.dropColumn("class_id");
        })
    }
}

