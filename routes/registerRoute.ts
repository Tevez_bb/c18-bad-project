import express from "express";
import { registerController } from "../main";
import { multerSingleImage } from "../util/multer";

const registerRoutes = express.Router();
registerRoutes.post("/register", multerSingleImage, registerController.registerAcc);

export default registerRoutes;