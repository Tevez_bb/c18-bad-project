import express from "express";
import { userInfoController } from "../main";

const userInfoRoutes = express.Router();

userInfoRoutes.get("/completedPose", userInfoController.getCompleted);

userInfoRoutes.get("/tryPose", userInfoController.getTry);

userInfoRoutes.get("/wishPose", userInfoController.getWish);

userInfoRoutes.get("/leader", userInfoController.getLeader);

export default userInfoRoutes;
