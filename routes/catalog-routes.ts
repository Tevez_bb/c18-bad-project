import express from "express";
import { catalogController } from "../main";

const catalogRoutes = express.Router();

catalogRoutes.get("/level", catalogController.getLevel);

catalogRoutes.get("/target", catalogController.getTarget);

catalogRoutes.get("/catalog", catalogController.getCatalog);

export default catalogRoutes;
