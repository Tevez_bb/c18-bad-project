import express from "express";
import { acUpdateController } from "../main";

const acUpdateRoutes = express.Router();
acUpdateRoutes.get("/account-update/:id", acUpdateController.getAccountDetail);
acUpdateRoutes.put("/account-update/:id", acUpdateController.updateAccountDetail);

export default acUpdateRoutes;