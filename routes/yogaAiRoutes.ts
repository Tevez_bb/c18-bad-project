import express from "express";

import { yogaAiController } from "../main";

const yogaAiRoutes = express.Router();

yogaAiRoutes.get("/posing", (req, res) => {
  res.redirect("/posing.html");
  // res.sendFile("/posing.html");
});

yogaAiRoutes.put("/bookmark", yogaAiController.toggleBookmark);

yogaAiRoutes.get("/poseDetail", yogaAiController.getPoseDetail);

yogaAiRoutes.post("/postHistory", yogaAiController.postHistory);

yogaAiRoutes.get("/checkBookmark", yogaAiController.checkBookmark)

export default yogaAiRoutes;
