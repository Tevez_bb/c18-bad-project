import express from "express";
import { userController } from "../main"
import { multerNone } from "../util/multer";

const userRoutes = express.Router();
userRoutes.post("/login", multerNone, userController.login);
userRoutes.post("/login", userController.login);
userRoutes.get("/login/google", userController.loginGoogle);
userRoutes.post("/logout", userController.logout)
userRoutes.get("/user", userController.getUser);

export default userRoutes;

// export function createUserRoutes(userController: UserController) {
//     const userRoutes = express.Router();

//     userRoutes.post("/logout", userController.logout);
//     userRoutes.get("/user", userController.getUser);
//     userRoutes.post("/login", userController.login);
//     userRoutes.get("/login/google", userController.loginGoogle);
//     return userRoutes;
// }
