import { Knex } from "knex";
import xlsx from "xlsx";
import { hashPassword } from "../util/hash";

interface User {
  email: string;
  name: string;
  password: string;
  experience_id: number;
  gender: string;
  age_group: string;
  icon: string;
}
interface userTarget {
  user_id: number;
  target_area_id: number;
  image?: string;
}
interface target {
  name: string;
}
interface experience {
  name: string;
}
interface pose {
  name: string;
  image: string;
  target_area: string;
  experience: string;
  class_no: number;
}
interface bookmark {
  user_id: number;
  pose_id: number;
}
interface history {
  completed: boolean;
  pose_id: number;
  user_id: number;
  accuracy: number;
}

let wb = xlsx.readFile("init_data.xlsx");
const userSheets = wb.Sheets["user"];
const users: User[] = xlsx.utils.sheet_to_json(userSheets);
const userTargetSheets = wb.Sheets["user_target_area"];
const userTargets: userTarget[] = xlsx.utils.sheet_to_json(userTargetSheets);
const targetSheets = wb.Sheets["target_area"];
const targets: target[] = xlsx.utils.sheet_to_json(targetSheets);
const experienceSheets = wb.Sheets["experience"];
const experiences: experience[] = xlsx.utils.sheet_to_json(experienceSheets);
const poseSheets = wb.Sheets["pose"];
const poses: pose[] = xlsx.utils.sheet_to_json(poseSheets);
const bookmarkSheets = wb.Sheets["bookmark"];
const bookmarks: bookmark[] = xlsx.utils.sheet_to_json(bookmarkSheets);
const historySheets = wb.Sheets["my_history"];
const historys: history[] = xlsx.utils.sheet_to_json(historySheets);

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("history").del();
  await knex("bookmark").del();
  await knex("user_target_area").del();
  await knex("pose").del();
  await knex("target_area").del();
  await knex("user").del();
  await knex("experience").del();

  // Inserts seed entries
  let insertExperienceIds: number[] = [];
  for (let experience of experiences) {
    let insertExperienceResult = await knex("experience")
      .returning("id")
      .insert({
        name: `${experience.name}`,
        created_at: "NOW()",
        updated_at: "NOW()",
      });
    // .first();
    insertExperienceIds.push(insertExperienceResult[0]);
  }
  console.table(insertExperienceIds);

  let insertUserIds: number[] = [];
  for (let userIndex = 0; userIndex < users.length; userIndex++) {
    const user = users[userIndex];
    let originalPassword = user.password;
    let hashedPassword = await hashPassword(originalPassword.toString());
    let insertUserResult = await knex("user")
      .returning("id")
      .insert({
        email: `${user.email}`,
        name: `${user.name}`,
        password: hashedPassword,
        experience_id:
          insertExperienceIds[userIndex % insertExperienceIds.length]["id"],
        gender: `${user.gender}`,
        age_group: `${user.age_group}`,
        icon: `${user.icon}`,
        created_at: "NOW()",
        updated_at: "NOW()",
      });
    // .first();
    insertUserIds.push(insertUserResult[0]);
  }

  let insertTargetIds: number[] = [];
  for (let target of targets) {
    let insertTargetResult = await knex("target_area").returning("id").insert({
      name: target.name,
      created_at: "NOW()",
      updated_at: "NOW()",
    });
    insertTargetIds.push(insertTargetResult[0]);
  }

  let insertPoseIds: number[] = [];
  for (let pose of poses) {
    let experience_id = await knex
      .select("id")
      .from("experience")
      .where("name", pose.experience)
      .first();
    console.log("experience_id = ", experience_id);

    let target_area_query = knex.select("id").from("target_area").where({
      name: pose.target_area,
    });

    console.log("sql >", target_area_query.toSQL());
    let target_area_id = await target_area_query;
    console.log("target_area_id = ", target_area_id[0]);

    let insertPoseResult = await knex("pose").returning("id").insert({
      name: pose.name,
      image: pose.image,
      target_area_id: target_area_id[0]["id"],
      experience_id: experience_id["id"],
      created_at: "NOW()",
      updated_at: "NOW()",
      class_id: pose.class_no,
    });
    insertPoseIds.push(insertPoseResult[0]);
  }

  for (
    let userTargetIndex = 0;
    userTargetIndex < userTargets.length;
    userTargetIndex++
  ) {
    await knex("user_target_area").insert({
      target_area_id:
        insertTargetIds[userTargetIndex % insertTargetIds.length]["id"],
      user_id: insertUserIds[userTargetIndex % insertUserIds.length]["id"],
      created_at: "NOW()",
      updated_at: "NOW()",
    });
  }

  for (
    let bookmarkIndex = 0;
    bookmarkIndex < bookmarks.length;
    bookmarkIndex++
  ) {
    await knex("bookmark").insert({
      user_id: insertUserIds[bookmarkIndex % insertUserIds.length]["id"],
      pose_id: insertPoseIds[bookmarkIndex % insertPoseIds.length]["id"],
      created_at: "NOW()",
      updated_at: "NOW()",
    });
  }

  for (let historyIndex = 0; historyIndex < poses.length; historyIndex++) {
    const history = historys[historyIndex];
    await knex("history").insert({
      completed: `${history.completed}`,
      pose_id: insertPoseIds[historyIndex % insertPoseIds.length]["id"],
      user_id: insertUserIds[historyIndex % insertUserIds.length]["id"],
      accuracy: `${history.accuracy}`,
      created_at: "NOW()",
      updated_at: "NOW()",
    });
  }
}
