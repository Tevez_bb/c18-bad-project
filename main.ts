import express from "express";
import { isLoggedIn } from "./middlewares/guard";
import { expressSessionGate,grantExpress, handle404 } from "./middlewares/app-middleware";
import { logger } from "./util/logger";
import multer from "multer";
import path from "path";
import env from "./env";
import Knex from "knex";
import { UserInfoController } from "./controllers/userInfo-controller";
import { UserInfoService } from "./services/userInfo-service";
import { CatalogController } from "./controllers/catalog-controller";
import { CatalogService } from "./services/catalog-service";
import { YogaAiController } from "./controllers/yogaAiController"; 
import { YogaAiService } from "./services/yogaAiService";
import { AcUpdateService } from "./services/acUpdateService";
import { AcUpdateController } from "./controllers/acUpdateController";


const knexConfig = require("./knexfile");
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();

// export const server = new http.Server(app);

app.use(express.json());
app.use(expressSessionGate);
app.use(grantExpress as express.RequestHandler);

let userInfoService = new UserInfoService(knex);
export let userInfoController = new UserInfoController(userInfoService);

import userInfoRoutes from "./routes/userInfo-routes";

app.use(userInfoRoutes);

let catalogService = new CatalogService(knex);
export let catalogController = new CatalogController(catalogService);
import catalogRoutes from "./routes/catalog-routes";
app.use(catalogRoutes);

let acUpdateService = new AcUpdateService(knex);
export let acUpdateController = new AcUpdateController(acUpdateService);
import acUpdateRoutes from "./routes/acUpdateRoute";
app.use(acUpdateRoutes);

import { UserController } from "./controllers/userController";
import { UserService } from "./services/userService";
let userService = new UserService(knex);
export let userController = new UserController(userService);
import userRoutes from "./routes/userRoute";
app.use(userRoutes);

import { RegisterService } from "./services/register-service";
import { RegisterController } from "./controllers/register-controller";
let registerService = new RegisterService(knex);
export let registerController = new RegisterController(registerService, userService);
import registerRoutes from "./routes/registerRoute";

app.use(registerRoutes);

let yogaAiService = new YogaAiService(knex);
export let yogaAiController = new YogaAiController(yogaAiService);
import yogaAiRoutes from "./routes/yogaAiRoutes";

app.use(yogaAiRoutes);

app.use(express.static("public"));
app.use(express.static("uploads"));
app.use("/admin", isLoggedIn, express.static("protected"));
app.use(handle404);

const storage = multer.diskStorage({
  destination: function (
    req: any,
    file: any,
    cb: (arg0: null, arg1: string) => void
  ) {
    cb(null, path.resolve("./uploads"));
  },
  filename: function (
    req: any,
    file: { fieldname: any; mimetype: string },
    cb: (arg0: null, arg1: string) => void
  ) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});

export const upload = multer({ storage });
// 教express 識睇 urlencoded 既req.body
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json({ message: "hi" });
});


app.post("/register", upload.single("icon"), async (req, res) => {});

app.listen(env.SERVER_PORT, () => {
  console.log(`Listening on http://localhost:${env.SERVER_PORT}/`);
  logger.info(`Listening on http://localhost:${env.SERVER_PORT}/`);
});
