let params = new URLSearchParams(document.location.search);
let userId = params.get("userId")
console.log(`userId`, userId);

// async function getUserInfo(){
//     let resUser = await fetch("/user");
//     let profileLists = await res.json();
//     let profileData = profileLists.data.user;
// }

// getUserInfo();
// console.log(`profileData`, profileData);

const registerForm = document.querySelector("#register-form");

let selectId = 1;

async function fetchTargetArea() {
    let res = await fetch(`/target`);
    const targetLists = await res.json();
    const targetSelectElem = document.querySelector('#targetAreaSelectUpdate');
    const userLists = await fetchUserString()
    console.log(userLists);
    for (let targetList of targetLists) {
        let newTargetString = getTargetString(userLists, targetList);
        // targetSelectElem.innerHTML += newTargetAreaString;
        targetSelectElem.innerHTML += newTargetString;
        selectId++;
}


async function fetchUserString(){
    let result = await fetch(`/account-update/:${userId}`)
    let userLists = await result.json();
    userLists = userLists.map(user => {
        return user.target_area_id
        
    })
    const userSelectElem = document.querySelector('.user-selected');
    return userLists
    console.log(`userLists`, userLists);
    // for (let userList of userLists){
    //  let newUserString = getUserString(userList);
    // }
}


function getTargetString(userLists, targetAreaList) {
    // console.log(`targetAreaList`, targetAreaList);
    // console.log(`userLists`, userLists);
    let targetAreaString;
    if ( userLists.includes(targetAreaList.id )) {   
    //     console.log("target area chosen");
        targetAreaString = `
        <input type="checkbox" class="btn-check" id="btncheck${selectId}" name="target_area_id" value=${targetAreaList.id} autocomplete="off" checked>
        <label class="btn btn-outline-secondary" for="btncheck${selectId}">${targetAreaList.name}</label>
        `;
    } else {
    //     console.log("no target area");
        targetAreaString = `
        <input type="checkbox" class="btn-check" id="btncheck${selectId}" name="target_area_id" value=${targetAreaList.id} autocomplete="off">
        <label class="btn btn-outline-secondary" for="btncheck${selectId}">${targetAreaList.name}</label>
        `;
    }
    return targetAreaString;
    }
}

fetchTargetArea();


registerForm.addEventListener("submit", async (e) => {
    e.preventDefault();
    let target_area_ids = document.querySelectorAll('input[name="target_area_id"]:checked');
    console.log(e)
    let values = [];
    for (let i = 0; i < target_area_ids.length; i++) {
        if (target_area_ids[i].checked) {
            values.push(target_area_ids[i].value);
        }
    }
    const formData = new FormData(registerForm);
    const json = Object.fromEntries(formData.entries());
    json.target_area_ids = values
    console.log(`json`, json);

    // formData.delete('target_area_id')
    // formData.append('target_area_id', JSON.stringify(values))
    const res = await fetch(`/account-update/${userId}`, {
        method: "put",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(json),
    });

    const parseRes = await res.json();
    if (res.ok) {
        console.log("updated");
        document.querySelector(".error-msg").innerText = parseRes.message;
        setTimeout(function(){location.href="/admin/userInfo.html"} , 3500);
    }
});


async function getLevel() {
    const res = await fetch("/level");
    const levelList = await res.json();
    const levelSelect = document.querySelector('#levelSelect');
    // console.log(levelSelect);
    for (let levelItem of levelList) {
        let levelOption = document.createElement("option");
        levelOption.value = levelItem.id;
        levelOption.innerHTML = levelItem.name;
        levelSelect.appendChild(levelOption);
    }
}
getLevel();