async function getUserInfo() {
  let res = await fetch("/user");
  let profileLists = await res.json();
  let profileData = profileLists.data.user;
  let searchTerm = "https";

  console.log(profileData);
  if (profileData.icon == "undefined" || profileData.icon == "" ) {
    console.log("no icon");
    document.querySelector(".info-box").innerHTML += `
      <div class="col userInfo headerElem">
      <img class="userIcon" src="./big-avatar.png" alt="">
      <div class="userText">
          <h2>${profileData.name}</h2>
      </div>
  </div>
      `;
  } else if (profileData.icon.indexOf(searchTerm) == 0) {
    console.log("google icon");
    document.querySelector(".info-box").innerHTML += `
      <div class="col userInfo headerElem">
      <img class="userIcon" src="${profileData.icon}" alt="">
      <div class="userText">
          <h2>${profileData.name}</h2>
      </div>
  </div>
      `;
  } else if (profileData.icon) {
    console.log("has icon");
    document.querySelector(".info-box").innerHTML += `
      <div class="col userInfo headerElem">
      <img class="userIcon" src="/${profileData.icon}" alt="">
      <div class="userText">
          <h2>${profileData.name}</h2>
      </div>
  </div>
      `;
  }
  document.querySelector(".bi-gear").addEventListener("click", function () {
    window.location.href = `./account-update.html?userId=${profileData.id}`;
  });
}

getUserInfo();

async function fetchTryPose() {
  let res = await fetch("/tryPose");
  let tryPoses = await res.json();
  let tryElem = document.querySelector(".try-container");
  for (let tryPose of tryPoses) {
    let newTryPoseString = getTryPoseString(tryPose);
    tryElem.innerHTML += newTryPoseString;
  }
  function getTryPoseString(tryPose) {
    let tryPoseHTMLString = `
  <div class="col">
  <div class="card h-100 product-card">
  <a href="./posing.html?poseId=${tryPose.pose_id}">
      <img src="/${tryPose.image}" class="card-img-top" alt="...">
    </a>
      <div class="card-body">
          <h5 class="card-title">${tryPose.pose_name}</h5>
          <p class="card-text">Target Area: ${tryPose.target_name}</p>
      </div>
  </div>
</div>
      `;
    return tryPoseHTMLString;
  }
}
fetchTryPose();

async function fetchCompletedPose() {
  let res = await fetch("/completedPose");
  let completedPoses = await res.json();
  let completedElem = document.querySelector(".completed-container");
  for (let completedPose of completedPoses) {
    let newCompletedPoseString = getCompletedPoseString(completedPose);
    completedElem.innerHTML += newCompletedPoseString;
  }
  function getCompletedPoseString(completedPose) {
    let completedPoseHTMLString = `
    <div class="col">
    <div class="card h-100 product-card">
    <a href="./posing.html?poseId=${completedPose.pose_id}">
        <img src="/${completedPose.image}" class="card-img-top" alt="...">
      </a>
        <div class="card-body">
            <h5 class="card-title">${completedPose.pose_name}</h5>
            <p class="card-text">Target Area: ${completedPose.target_name}</p>
            <p class="score-text">Completed Time: ${completedPose.total_time}s</p>
        </div>
    </div>
  </div>
        `;
    return completedPoseHTMLString;
  }
}
fetchCompletedPose();

async function fetchWishPose() {
  let res = await fetch("/wishPose");
  let wishPoses = await res.json();
  let wishElem = document.querySelector(".wish-container");
  for (let wishPose of wishPoses) {
    let newWishPoseString = getWishPoseString(wishPose);
    wishElem.innerHTML += newWishPoseString;
  }
  function getWishPoseString(wishPose) {
    let wishPoseHTMLString = `
      <div class="col">
      <div class="card h-100 product-card">
      <a href="./posing.html?poseId=${wishPose.pose_id}">
          <img src="/${wishPose.image}" class="card-img-top" alt="...">
        </a>
          <div class="card-body">
              <h5 class="card-title">${wishPose.pose_name}</h5>
              <p class="card-text">Target Area: ${wishPose.target_name}</p>
          </div>
      </div>
    </div>
          `;
    return wishPoseHTMLString;
  }
}
fetchWishPose();
