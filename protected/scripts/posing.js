const enableWebcamButton = document.getElementById("webcamButton");
const disableWebcamButton = document.getElementById("webcamDisableButton");
const demosSection = document.getElementById("demos");
const poseTimerDisplay = document.getElementById("timer_seconds");
const poseAccuracyDisplay = document.getElementById("model_response");
const video = document.getElementById("webcam");

const instructionText = document.querySelector(".instruction");
const stepInstruction = document.querySelector(".steps");
const fps = document.querySelector(".fpsSection");
const poseNameHTML = document.querySelector(".pose_info");
const poseImageHTML = document.querySelector(".pose_img");
const poseAreaHTML = document.querySelector(".pose_area");
const toBookmark = document.querySelector(".toBookmark");

let startRightPoseTime;
// flag variable is used to help capture the time when AI just detect
// the pose as correct(probability more than threshold)
let defaultColour = "rgb(255,255,255)";
let rightPoseColour = "#FB6855";
let skeletonColor = defaultColour;
let fpsDIY = 5;
let trainingTimeThreshold = 5;

let flag = false;
let classify_threshold = 0.9;
let poseTimer = 1;
let timerStart = false;
let bookmarked = false;

let notDetected = 0;
let notDetectedPose = 0;

const CLASS_NO = {
  boat_pose: 0,
  bound_angle: 1,
  bow_pose: 2,
  cat_pose: 3,
  chair_pose: 4,
  child_pose: 5,
  cobra: 6,
  cow_face_pose: 7,
  downward_facing_dog: 8,
  eagle_pose: 9,
  extended_hand_to_big_toe_pose: 10,
  extended_side_angle_pose: 11,
  fish_pose: 12,
  happy_baby: 13,
  head_to_knee_pose: 14,
  hero_pose: 15,
  lotus: 16,
  low_lunge: 17,
  noose_pose: 18,
  plank: 19,
  puppy_pose_deep_backbend: 20,
  seat_forward_bend: 21,
  spinal_twist: 22,
  standing_forward_bend: 23,
  tree_pose: 24,
  two_legged_inverted_staff_pose: 25,
  warrior_I: 26,
  warrior_II: 27,
  warrior_III: 28,
  wide_angle_seat_forward_bend: 29,
};

const POINTS = {
  NOSE: 0,
  LEFT_EYE: 1,
  RIGHT_EYE: 2,
  LEFT_EAR: 3,
  RIGHT_EAR: 4,
  LEFT_SHOULDER: 5,
  RIGHT_SHOULDER: 6,
  LEFT_ELBOW: 7,
  RIGHT_ELBOW: 8,
  LEFT_WRIST: 9,
  RIGHT_WRIST: 10,
  LEFT_HIP: 11,
  RIGHT_HIP: 12,
  LEFT_KNEE: 13,
  RIGHT_KNEE: 14,
  LEFT_ANKLE: 15,
  RIGHT_ANKLE: 16,
};

const keypointConnections = {
  nose: ["left_ear", "right_ear"],
  left_ear: ["left_shoulder"],
  right_ear: ["right_shoulder"],
  left_shoulder: ["right_shoulder", "left_elbow", "left_hip"],
  right_shoulder: ["right_elbow", "right_hip"],
  left_elbow: ["left_wrist"],
  right_elbow: ["right_wrist"],
  left_hip: ["left_knee", "right_hip"],
  right_hip: ["right_knee"],
  left_knee: ["left_ankle"],
  right_knee: ["right_ankle"],
};
function drawSegment(ctx, [mx, my], [tx, ty], color) {
  ctx.beginPath();
  ctx.moveTo(mx, my);
  ctx.lineTo(tx, ty);
  ctx.lineWidth = 5;
  ctx.strokeStyle = color;
  ctx.stroke();
}

function drawPoint(ctx, x, y, r, color) {
  ctx.beginPath();
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.fillStyle = color;
  ctx.fill();
}
// just Checking what js is loaded at the front end
console.log(
  `Fetching, modelV3.json ,classify_threshold: ${classify_threshold}`
);

//for Al Classification to read relevant Ai Class ID
async function fetchRequestedPoseToAiID() {
  let params = new URLSearchParams(document.location.search);
  // console.log("route:",`/poseDetail?`+ params.toString());
  let res = await fetch(`/poseDetail?` + params.toString());
  // console.log("result from ser: ",res);

  if (res.ok) {
    let resForAiId = await res.json();
    let modelClassId = resForAiId[0]["class_id"];
    return modelClassId;
  } else {
    console.log("unknown response from server: ", res);
  }
}

//Fetch pose info to display
async function fetchPoseInfo() {
  console.log("try to fetch pose info");
  let params = new URLSearchParams(document.location.search);
  let res = await fetch(`/poseDetail?` + params.toString());
  // console.log("result from ser: ",res)
  if (res.ok) {
    let resForDetails = await res.json();
    poseImageHTML.innerHTML = `<img src="../${resForDetails[0].image}" class="card-img-top" alt="..."></img>`;
    poseNameHTML.textContent = resForDetails[0]["pose_name"];
    poseAreaHTML.textContent =
      "Target Area: " + resForDetails[0]["target_name"];
  }
}
//Run at once to display the details of the pose
fetchPoseInfo();

//Let the button to fetch and save bookmark
async function fetchToBookmark() {
  let paramsToBookmark = new URLSearchParams(document.location.search);
  if (!bookmarked) {
    await fetch(`/bookmark?` + paramsToBookmark.toString(), {
      method: "PUT",
      headers: { "Content-Type": "application/json; charset=utf-8" },
    });
    toBookmark.classList.add("toBookmark_whenClicked");
    bookmarked = true;
  } else {
    await fetch(`/bookmark?` + paramsToBookmark.toString(), { method: "PUT" });
    toBookmark.classList.remove("toBookmark_whenClicked");
    bookmarked = false;
  }
}

async function fetchToCheckBookmarked() {
  let paramsToCheckBookmark = new URLSearchParams(document.location.search);
  let res = await fetch(`/checkBookmark?` + paramsToCheckBookmark.toString());
  let poseBookmarkChecked = await res.json();
  if (poseBookmarkChecked) {
    toBookmark.classList.add("toBookmark_whenClicked");
    bookmarked = true;
  }
}

fetchToCheckBookmarked();
// preprocessing
{
  function get_center_point(landmarks, left_bodypart, right_bodypart) {
    let left = tf.gather(landmarks, left_bodypart, 1);
    let right = tf.gather(landmarks, right_bodypart, 1);
    const center = tf.add(tf.mul(left, 0.5), tf.mul(right, 0.5));
    return center;
  }

  function get_pose_size(landmarks, torso_size_multiplier = 2.5) {
    let hips_center = get_center_point(
      landmarks,
      POINTS.LEFT_HIP,
      POINTS.RIGHT_HIP
    );
    let shoulders_center = get_center_point(
      landmarks,
      POINTS.LEFT_SHOULDER,
      POINTS.RIGHT_SHOULDER
    );
    let torso_size = tf.norm(tf.sub(shoulders_center, hips_center));
    let pose_center_new = get_center_point(
      landmarks,
      POINTS.LEFT_HIP,
      POINTS.RIGHT_HIP
    );
    pose_center_new = tf.expandDims(pose_center_new, 1);

    pose_center_new = tf.broadcastTo(pose_center_new, [1, 17, 2]);
    // return: shape(17,2)
    let d = tf.gather(tf.sub(landmarks, pose_center_new), 0, 0);
    let max_dist = tf.max(tf.norm(d, "euclidean", 0));

    // normalize scale
    let pose_size = tf.maximum(
      tf.mul(torso_size, torso_size_multiplier),
      max_dist
    );
    return pose_size;
  }

  function normalize_pose_landmarks(landmarks) {
    let pose_center = get_center_point(
      landmarks,
      POINTS.LEFT_HIP,
      POINTS.RIGHT_HIP
    );
    pose_center = tf.expandDims(pose_center, 1);
    pose_center = tf.broadcastTo(pose_center, [1, 17, 2]);
    landmarks = tf.sub(landmarks, pose_center);

    let pose_size = get_pose_size(landmarks);
    landmarks = tf.div(landmarks, pose_size);
    return landmarks;
  }

  function landmarks_to_embedding(landmarks) {
    // normalize landmarks 2D
    landmarks = normalize_pose_landmarks(tf.expandDims(landmarks, 0));
    let embedding = tf.reshape(landmarks, [1, 34]);
    return embedding;
  }
  /////////////////////////
}

// Only run once when the start button is clicked
const initializeModel = async () => {
  enableWebcamButton.classList.add("noplace");
  stepInstruction.classList.add("noplace");
  instructionText.classList.remove("titleFeature");
  instructionText.textContent = "Setting up... ";
  let currentPoseID = await fetchRequestedPoseToAiID();
  // console.log("currentPoseID: ", currentPoseID);

  // Setting up pose detection model for returning key-points for our DIY classification model
  const modelToUse = poseDetection.SupportedModels.MoveNet;

  // If running MoveNet_thunder,which is more accurate pose detecting yet slower,
  // otherwise delete { modelType: poseDetection.movenet.modelType.SINGLEPOSE_THUNDER };
  const model = await poseDetection.createDetector(
    poseDetection.SupportedModels.MoveNet,
    { modelType: poseDetection.movenet.modelType.SINGLEPOSE_THUNDER }
  );

  instructionText.textContent = "Model is loading...";
  const poseClassifier = await tf.loadLayersModel("./model/modelV3.json");
  document.querySelector(".detect_accuracy").classList.remove("unseen");
  document.querySelector(".timer").classList.remove("unseen");
  // fps.classList.remove("unseen");
  document.querySelector(".fps").textContent = fpsDIY;
  instructionText.textContent = "Operating..";

  interval = setInterval(() => {
    app(model, poseClassifier, currentPoseID);
  }, 1000 / fpsDIY);

  timer = setInterval(() => {
    if (timerStart) {
      console.log("+pretending timer: ", poseTimer);
      poseTimer++;
      poseTimerDisplay.innerText = `${poseTimer}s`;
    }

    // else{
    //     console.log("0_pretending timer: ", poseTimer)
    //     // poseTimer+=0;
    //     poseTimerDisplay.innerText = `${poseTimer}s`
    // }
  }, 1000);
};

// Keep running to let the canvas function draw the result from  MoveNet Pose Detection and our Classification Model.
async function app(model, poseClassifier, currentPoseID) {
  const webcamElement = document.getElementById("webcam");
  const canvas = document.getElementById("detect_result");
  const context = canvas.getContext("2d");
  const color = ["green", "yellow", "red"];

  let checkingEstimateOn = true;
  let showResult = async function () {
    if (checkingEstimateOn) {
      // console.log("Estimating poses")
      checkingEstimateOn = false;
    }
    const result = await model.estimatePoses(webcamElement);
    canvas.width = webcamElement.videoWidth;
    canvas.height = webcamElement.videoHeight;
    context.drawImage(webcamElement, 0, 0);
    context.font = "40px Arial";
    // console.log("MoveNet result: ", result[0])
    // result = pose keypoints
    // console.log("not detect: ", notDetected)
    if (result.length) {
      try {
        const pose = result[0];
        const keypoints = pose.keypoints;
        let input = keypoints.map((keypoint) => {
          if (keypoint.score > 0.4) {
            if (
              !(keypoint.name === "left_eye" || keypoint.name === "right_eye")
            ) {
              drawPoint(context, keypoint.x, keypoint.y, 8, `${defaultColour}`);
              let connections = keypointConnections[keypoint.name];
              try {
                connections.forEach((connection) => {
                  let conName = connection.toUpperCase();
                  drawSegment(
                    context,
                    [keypoint.x, keypoint.y],
                    [
                      keypoints[POINTS[conName]].x,
                      keypoints[POINTS[conName]].y,
                    ],
                    skeletonColor
                  );
                });
              } catch (err) {}
            }
            notDetected -= 1;
          } else {
            notDetected += 1;
          }
          return [keypoint.x, keypoint.y];
        });
        if (notDetected > fpsDIY) {
          skeletonColor = `${defaultColour}`;
          // notDetected = 0;
          // poseAccuracyDisplay.textContent = `No right pose detected`
          // poseTimerDisplay.innerText = "Not holding long enough";
          // poseTimer = 0;
          return;
        }
        // console.log("network:", input)

        // Start classification DIY model
        const processedInput = await landmarks_to_embedding(input);
        const classification = poseClassifier.predict(processedInput);
        // code for checking

        classification.array().then(async (data) => {
          const classNo = CLASS_NO[currentPoseID];
          console.log("Accuracy: ", data[0][currentPoseID]);
          // data[0][classNo] = Accuracy
          // = how close the classification model thinks your pose is
          // to the trained and set pose

          if (data[0][currentPoseID] > classify_threshold) {
            if (!flag) {
              flag = true;
            }
            // Posing is right, and shall start a timer
            skeletonColor = `${rightPoseColour}`;
            if (timerStart == false) {
              // console.log("right");
              startRightPoseTime = new Date().getTime();
              // console.log("timerStart right", startRightPoseTime)
              timerStart = true;
            }

            if (typeof startRightPoseTime == Number) {
              let checkOneSecond = new Date().getTime();
              let delta = (checkOneSecond - startRightPoseTime) / 1000;
              // console.log("time delta: ", delta)
              // if(delta){}
            }

            let poseCheck = (data[0][currentPoseID] * 100).toFixed(1);

            // console.log("checking acc: ", poseCheck)
            if (poseCheck) {
              poseAccuracyDisplay.textContent = `${poseCheck}%`;
            }
          } else {
            poseAccuracyDisplay.textContent = `No Right Pose Detected`;
            flag = false;
            poseTimerDisplay.innerText = "Not Holding long enough";
            poseTimer = 1;
            // Posing is NOT correct, and shall STOP a timer
            skeletonColor = "rgb(255,255,255)";
            // countAudio.pause()
            // countAudio.currentTime = 0
            const endRightPoseTime = new Date();
            if (timerStart) {
              // console.log("starting at: ",startRightPoseTime)
              let rightPosingTime =
                (endRightPoseTime.getTime() - startRightPoseTime) / 1000;
              console.log(`Right Posing Time: ${rightPosingTime}s`);
              timerStart = false;
              if (rightPosingTime > trainingTimeThreshold) {
                console.log("Really fetch and store record");
                let paramsToRecord = new URLSearchParams(
                  document.location.search
                );
                await fetch("/postHistory?" + paramsToRecord.toString(), {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json; charset=utf-8",
                  },
                  body: JSON.stringify({
                    accuracy: `${Math.round(rightPosingTime)}`,
                  }),
                });
                // console.log("trying to fetch and store record")
              }
            }
          }
        });
      } catch (error) {}
    }
  };

  // WebCam section
  let setupWebcam = function () {
    return new Promise((resolve, reject) => {
      const navigatorAny = navigator;
      navigator.mediaDevices.getUserMedia =
        navigator.mediaDevices.getUserMedia ||
        navigatorAny.mediaDevices.webkitGetUserMedia ||
        navigatorAny.mediaDevices.mozGetUserMedia ||
        navigatorAny.mediaDevices.msGetUserMedia;
      if (navigator.mediaDevices.getUserMedia) {
        navigator.getUserMedia(
          {
            video: true,
          },
          (stream) => {
            webcamElement.srcObject = stream;
            webcamElement.addEventListener(
              "loadeddata",
              () => resolve(),
              false
            );
          },
          (err) => reject(err)
        );
      } else {
        reject("getUserMedia failed");
      }
    });
  };
  setupWebcam().then(
    () => {
      instructionText.textContent = "WebCam Operating";
      instructionText.classList.add("noplace");
      // console.log("WebCam Operating")
      showResult();
    },
    (err) => {
      console.log(err);
    }
  );

  // backup1
}

enableWebcamButton.addEventListener("click", initializeModel);
// toBookmark.addEventListener('click',()=>{
//     toBookmark.classList.add("");
//     console.log("to bookmark");
// })

// backup1
// const navigatorAny = navigator;
// navigator.mediaDevices.getUserMedia
// = navigator.mediaDevices.getUserMedia ||
// navigatorAny.mediaDevices.webkitGetUserMedia ||
// navigatorAny.mediaDevices.mozGetUserMedia ||
// navigatorAny.mediaDevices.msGetUserMedia;
// let streamingCamVideo = await navigator.mediaDevices.getUserMedia({ audio: false, video: true });
// function toCatchPoseClassify(){
//     instructionText.textContent = "WebCam Operating"
//     showResult();
// }
// if (streamingCamVideo) {

//         webcamElement.srcObject = streamingCamVideo;
//         webcamElement.addEventListener('loadeddata',toCatchPoseClassify());

// }

// backup2
// let setupWebcam = function () {
//     return new Promise((resolve, reject) => {
//         const navigatorAny = navigator;
//         navigator.mediaDevices.getUserMedia = navigator.mediaDevices.getUserMedia ||
//             navigatorAny.mediaDevices.webkitGetUserMedia || navigatorAny.mediaDevices.mozGetUserMedia ||
//             navigatorAny.mediaDevices.msGetUserMedia;
//         if (navigator.mediaDevices.getUserMedia) {
//             navigator.mediaDevices.getUserMedia({
//                 video: true
//             },
//                 (stream) => {
//                     webcamElement.srcObject = stream;
//                     webcamElement.addEventListener('loadeddata', () => resolve(),
//                         false);
//                 },
//                 (err) => reject(err));
//         } else {
//             reject("getUserMedia failed");
//         }
//     });
// }
// setupWebcam().then(
//     () => {
//         instructionText.textContent = "WebCam Operating";
//         instructionText.classList.add("noplace");
//         // console.log("WebCam Operating")
//         showResult();
//     },
//     (err) => {
//         console.log(err);
//     }
// );

// Slow
// const navigatorAny = navigator;
// navigator.mediaDevices.getUserMedia = navigator.mediaDevices.getUserMedia ||
//     navigatorAny.mediaDevices.webkitGetUserMedia || navigatorAny.mediaDevices.mozGetUserMedia ||
//     navigatorAny.mediaDevices.msGetUserMedia;
// if (navigator.mediaDevices.getUserMedia) { }
// navigator.mediaDevices.getUserMedia({ video: true })
//     .then(function (stream) {
//         /* use the stream */
//         webcamElement.srcObject = stream;

//         instructionText.textContent = "WebCam Operating";
//         instructionText.classList.add("noplace");

//         webcamElement.addEventListener('loadeddata', showResult, false);
//     })
//     .catch(function (err) {
//         /* handle the error */
//         console.error(err)
//     });
