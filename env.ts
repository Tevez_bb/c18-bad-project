import dotenv from "dotenv";

dotenv.config();

const env = {
  DB_NAME: process.env.DB_NAME || "yoga-Ai",
  DB_USER: process.env.DB_USERNAME || "carlosleung",
  DB_PASSWORD: process.env.DB_PASSWORD || "carlosleung",
  TEST_DB_NAME: process.env.DB_NAME || "yoga-Ai-test",
  TEST_DB_USER: process.env.DB_USERNAME || "carlosleung",
  TEST_DB_PASSWORD: process.env.DB_PASSWORD || "carlosleung",
  GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
  SERVER_PORT: process.env.SERVER_PORT || "8080",
  NODE_ENV: "development",
  GOOGLE_ORIGIN: process.env.GOOGLE_ORIGIN || "http://localhost:8080",
};

export default env;
