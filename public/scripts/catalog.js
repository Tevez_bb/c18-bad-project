async function getLevels() {
  const res = await fetch("/level");
  const levelsList = await res.json();
  //   const levelsList = resObj.data;
  //   console.log("resObj=", resObj);
  console.log("levelsList =", levelsList);
  const levelsSelect = document.querySelector(".level-list");
  for (let levelsItem of levelsList) {
    let opt = document.createElement("div");
    opt.className = "list-group-item list-group-item-action my-list";
    let params = new URLSearchParams(document.location.search);
    if (params.has("levelId")) {
      const levelId = params.get("levelId");
      if (levelsItem.id == levelId) {
        opt.className += " clicked";
      }
    }
    opt.innerHTML = levelsItem.name;
    opt.onclick = () => toggleTarget("levelId", levelsItem.id);
    levelsSelect.appendChild(opt);
  }
}

getLevels();

async function getTargets() {
  const res = await fetch("/target");
  const targetsList = await res.json();
  //   const targetsList = resObj.data;
  const targetsSelect = document.querySelector(".target-list");
  for (let targetsItem of targetsList) {
    let opt = document.createElement("div");
    opt.className = "list-group-item list-group-item-action my-list";
    let params = new URLSearchParams(document.location.search);
    if (params.has("targetId")) {
      const targetId = params.get("targetId");
      if (targetsItem.id == targetId) {
        console.log("targetId : ", targetId);
        console.log("targetsItem.id : ", targetsItem.id);
        opt.className += " clicked";
        console.log("opt.className : ", opt.className);
      }
    }

    // opt.href = `/catalog.html?targetId=${targetsItem.id}`;
    opt.innerHTML = targetsItem.name;
    opt.onclick = () => toggleTarget("targetId", targetsItem.id);
    targetsSelect.appendChild(opt);
  }
}

getTargets();

function toggleTarget(type, id) {
  //  click (id)=> getItem('level') & getItem('target') => []checking id is existing=> setItem([..., id]) => url => ....?level=[..., id]
  let params = new URLSearchParams(document.location.search);
  console.log(params.get("levelId"));
  if (type === "targetId") {
    if (params.has("targetId") && params.get("targetId") == id) {
      params.delete("targetId");
    } else {
      params.set("targetId", id);
    }
  }
  if (type === "levelId") {
    if (params.has("levelId") && params.get("levelId") == id) {
      params.delete("levelId");
    } else {
      params.set("levelId", id);
    }
  }

  let result = window.location.origin + "/catalog.html?" + params.toString();

  window.location.replace(result);
}

async function fetchCatalog() {
  let params = new URLSearchParams(document.location.search);
  let res = await fetch(`/catalog?` + params.toString());
  let catalogsList = await res.json();
  let catalogContainerElem = document.querySelector(".catalog-container");

  for (let catalogsLists of catalogsList) {
    let newCatalogsString = getCatalogsHTMLString(catalogsLists);
    catalogContainerElem.innerHTML += newCatalogsString;
  }

  function getCatalogsHTMLString(catalogsLists) {
    let catalogHTMLString = `
      <div class="col">
      <div class="card h-100 product-card">
      <a href="/admin/posing.html?poseId=${catalogsLists.pose_id}">
          <img src="${catalogsLists.image}" class="card-img-top" alt="...">
          </a>
          <div class="card-body">
              <h5 class="card-title">${catalogsLists.pose_name}</h5>
              <p class="card-text">Target area: ${catalogsLists.target_name}</p>
          </div>
      </div>
  </div>
            `;
    return catalogHTMLString;
  }
}

fetchCatalog();

// async function checkIds() {
//   let params = new URLSearchParams(document.location.search);
//   console.log(params.has("targetId"));
//   setTimeout(() => {
//     if (params.has("targetId")) {
//       document.querySelector(".target-small-list").classList.add("clicked");
//     } else {
//       document.querySelector(".target-small-list").classList.remove("clicked");
//     }
//   }, 100);
// }

// checkIds();
