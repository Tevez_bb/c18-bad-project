const registerForm = document.querySelector("#register-form");
const registerResultElem = document.querySelector("#register-result");

registerForm.addEventListener("submit", async (e) => {
    e.preventDefault();
    // let registerFormObj = {
    //     name: registerForm.name.value,
    //     email: registerForm.email.value,
    //     password: registerForm.password.value,
    //     confirm_password: registerForm.confirm_password.value,
    //     gender: registerForm.gender.value,
    //     experience_id: registerForm.experience_id.value,
    //     age_group: registerForm.age_group.value,
    //     //  target_area_id: registerForm.target_area_id.value,
    // }
    let target_area_ids = document.querySelectorAll('input[name="target_area_id"]:checked');
    // console.log(e)
    let values = [];
    for (let i = 0; i < target_area_ids.length; i++) {
        if (target_area_ids[i].checked) {
            values.push(target_area_ids[i].value);
        }
    }
    // registerFormObj.target_area_id = JSON.stringify(values);
    // console.log(registerFormObj);

    const formData = new FormData(registerForm);
    formData.delete('target_area_id')
    formData.append('target_area_id', JSON.stringify(values))
    // formData.append('name', registerForm.name.value)
    // formData.append('email', registerForm.email.value)
    // formData.append('password', registerForm.password.value)
    // formData.append('confirm_password', registerForm.confirm_password.value)
    // formData.append('gender', registerForm.gender.value)
    // formData.append('experience_id', registerForm.experience_id.value)
    // formData.append('age_group', registerForm.age_group.value)

    const res = await fetch("/register", {
        method: "post",
        body: formData,
    });

    const parseRes = await res.json();

    // if (!parseRes.success) {
    //     registerResultElem.innerText = parseRes.message;
    //     console.log(`parseRes.message`, parseRes.message);

    //     window.location.replace = "/";
    console.log(res.status)

    if (res.status == 200) {
        console.log("test")
        // registerResultElem.innerText = parseRes.message;
        // console.log(`parseRes.message`, parseRes.message);
        // window.location.href = "/"
        window.location.replace("/")    
    } else if(!parseRes.success) {
        registerResultElem.innerText = parseRes.message;
        console.log(`parseRes.message`, parseRes.message);
    } else {
        const email = e.target.email.value;
        const password = e.target.password.value;
        await fetch("/login", {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
    }
});

async function getLevel() {
    const res = await fetch("/level");
    const levelList = await res.json();
    const levelSelect = document.querySelector('#levelSelect');
    // console.log(levelSelect);
    for (let levelItem of levelList) {
        let levelOption = document.createElement("option");
        levelOption.value = levelItem.id;
        levelOption.innerHTML = levelItem.name;
        levelSelect.appendChild(levelOption);
    }
}
getLevel();

let selectId = 1;
async function getTargetArea() {
    const res = await fetch("/target");
    const targetAreaLists = await res.json();
    const targetSelectElem = document.querySelector('#targetAreaSelect');

    for (let targetAreaList of targetAreaLists) {
        let newTargetAreaString = getTargetAreaString(targetAreaList);
        targetSelectElem.innerHTML += newTargetAreaString;
        selectId++;
    }
}

function getTargetAreaString(targetAreaList) {
    let targetAreaString = `
      <input type="checkbox" class="btn-check" id="btncheck${selectId}" name="target_area_id" value=${targetAreaList.id} autocomplete="off">
      <label class="btn btn-outline-secondary" for="btncheck${selectId}">${targetAreaList.name}</label>
      `;
    return targetAreaString;
}

getTargetArea();