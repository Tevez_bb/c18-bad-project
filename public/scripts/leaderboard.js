async function fetchLeaderBoard() {
  let res = await fetch(`/leader`);
  let leaderBoardLists = await res.json();
  let leaderContainerElem = document.querySelector(".leader-list");

  for (let leaderBoardList of leaderBoardLists) {
    let newLeaderString = getLeaderHTMLString(leaderBoardList);
    leaderContainerElem.innerHTML += newLeaderString;
  }

  function getLeaderHTMLString(leaderBoardList) {
    let leaderHTMLString = `
    <li>
    <mark>${leaderBoardList.name}</mark>
    <small>${leaderBoardList.counter}</small>
</li>
              `;
    return leaderHTMLString;
  }
}

fetchLeaderBoard();
