// $(document).ready(function(){
//   $(".owl-carousel").owlCarousel();
// });

async function fetchUser() {
  let res = await fetch("/user");
  if (res.ok) {
    let result = await res.json();
    console.log(`result`, result);
    let resultData = result.data;
    let searchTerm = "https";
    document.querySelector(".avatar").innerHTML = "";
    document.querySelector(".avatar").removeAttribute("hidden");
    document.querySelector(".btn-logout").removeAttribute("hidden");
    document.querySelector(".btn-login").setAttribute("hidden", "");
    if (resultData.user.icon == "undefined" || resultData.user.icon == "") {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<a href="/admin/userInfo.html">
      <img src="./assets/default-avatar.png" class="avatar-style"></img>
      </a>`;
    } else if (resultData.user.icon.indexOf(searchTerm) == 0) {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<a href="/admin/userInfo.html">
      <img src="${resultData.user.icon}" class="avatar-style"></img>
      </a>`;
    } else if (resultData.user.icon) {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<a href="/admin/userInfo.html">
      <img src="/${resultData.user.icon}" class="avatar-style"></img>
      </a>`;
    } else {
      document.querySelector(".btn-login").removeAttribute("hidden");
      document.querySelector(".avatar").setAttribute("hidden", "");
      document.querySelector(".btn-logout").setAttribute("hidden", "");
      // document.querySelector(".wishlist").setAttribute("hidden", "");
    }
  }
}
fetchUser();

document.querySelector(".btn-login").addEventListener("click", function () {
  window.location.href = "login.html";
});
document.querySelector(".btn-discover").addEventListener("click", function () {
  window.location.href = "catalog.html";
});
document.querySelector(".btn-logout").addEventListener("click", async (e) => {
  e.preventDefault();
  // let logoutElem = e.currentTarget;
  let res = await fetch("/logout", {
    method: "POST",
  });
  if (res.ok) {
    console.log("logged out");
    let result = await res.json();
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
    window.location.href = "/";
  }
});
document.querySelector(".header-logo").addEventListener("click", function () {
  window.location.href = "/";
});

document.querySelector(".search-btn").addEventListener("click", function () {
  let searchKey = document.querySelector(".search-item").value;
  let params = new URLSearchParams(document.location.search);
  params.set("search", searchKey);
  console.log(params.get("search"));
  let result = "./catalog.html?" + params.toString();
  window.location.href = result;
});
