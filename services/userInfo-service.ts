import { Knex } from "knex";

export class UserInfoService {
  constructor(private knex: Knex) {}

  async getTryPose(user_id: number) {
    return await this.knex
      .select(
        "pose.id as pose_id",
        "pose.name as pose_name",
        "pose.image",
        "target_area.name as target_name"
      )
      .from("user")
      .innerJoin("user_target_area", "user_target_area.user_id", "user.id")
      .innerJoin(
        "pose",
        "pose.target_area_id",
        "user_target_area.target_area_id"
      )
      .innerJoin("target_area", "target_area.id", "pose.target_area_id")
      .where("user.id", user_id)
      .andWhere("user.experience_id", "=", this.knex.ref("pose.experience_id"));
  }

  async getCompletedPose(user_id: number) {
    return (
      (
        await //     .select(
        //       "history.accuracy",
        //       "pose.id as pose_id",
        //       "pose.name as pose_name",
        //       "pose.image",
        //       "target_area.name as target_name"
        //     )
        //     .from("history")
        //     .innerJoin("pose", "pose.id", "history.pose_id")
        //     .innerJoin("user", "user.id", "history.user_id")
        //     .innerJoin("target_area", "target_area.id", "pose.target_area_id")
        //     .where("user.id", user_id)
        //     .andWhere("history.completed", "true");
        this.knex
          .raw(`select sum(accuracy) as total_time, pose_id , pose."name" as pose_name, pose.image, ta."name" as target_name  

        from history h 
        inner join pose on pose.id = h.pose_id 
        inner join "user" u on u.id = h.user_id
        inner join target_area ta on ta.id = pose.target_area_id
        where u.id = ${user_id}
        and h.completed  = true
        group by u.id , pose_id , pose_name , pose.image, ta."name"`)
      ).rows
    );
  }

  async getWishPose(user_id: number) {
    return await this.knex
      .select(
        "pose.id as pose_id",
        "pose.name as pose_name",
        "pose.image",
        "target_area.name as target_name"
      )
      .from("bookmark")
      .innerJoin("pose", "pose.id", "bookmark.pose_id")
      .innerJoin("user", "user.id", "bookmark.user_id")
      .innerJoin("target_area", "target_area.id", "pose.target_area_id")
      .where("user.id", user_id);
  }

  async getCompletedNum() {
    return (
      await this.knex.raw(`
        select "user".name , count(distinct(pose_id)) as counter from history h 
        inner join "user" on "user".id  = h.user_id 
        where completed =true 
        group by (h.user_id , "user".name)
        order by counter 
        desc limit 5
        `)
    ).rows;
  }
}
