import { Knex } from "knex";
import { hashPassword } from "../util/hash";

export class RegisterService {
  constructor(private knex: Knex) {}

  public async register(
    email: string,
    name: string,
    password: string,
    icon: string,
    experience_id: number,
    gender: string,
    age_group: string,
    target_area_ids: number[]
  ): Promise<number> {
    let originalPassword = password;
    let hashedPassword = await hashPassword(originalPassword.toString());
    // console.log({email, name, password:hashedPassword, icon, gender, experience_id, age_group})

    const userResult = await this.knex("user")
      .returning("id")
      .insert({
        email,
        name,
        password: hashedPassword,
        icon,
        gender,
        experience_id,
        age_group,
      });
    // for (let target_area_id of target_area_ids){
    // console.log(`user id`, user_id);
    const user_id = userResult[0]["id"];
    console.log(user_id);

    // }
    console.log(target_area_ids);
    let targetAreaInfo = target_area_ids.map((target_area_id) => {
      return { user_id, target_area_id };
    });
    console.log(targetAreaInfo);

    await this.knex("user_target_area").insert(targetAreaInfo);
    // .then(() => { /* handle success */ })
    // .catch((e) => {console.log(e);
    // });
    return user_id;
  }

  // public async loginAfterRegister(email: string, password:string) {
  //     let foundUser = await this.knex("user").select("*").where(email, password);
  //     return foundUser;
  // }
}
