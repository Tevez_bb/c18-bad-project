import { Knex } from "knex";
import { Target } from "../model";

export class CatalogService {
  constructor(private knex: Knex) {}

  async getAllLevels() {
    return await this.knex.select("*").from("experience").orderBy("id", "desc");
  }

  async getAllTargets(): Promise<Target[]> {
    return await this.knex
      .select("*")
      .from("target_area")
      .orderBy("id", "desc");
  }

  async getCatalogItems(
    targetId: number | undefined,
    levelId: number | undefined,
    search: string
  ) {
    // console.log("tagetId=", targetId);
    // console.log("levelId=", levelId);

    let query = this.knex
      .select(
        "pose.id as pose_id",
        "pose.name as pose_name",
        "pose.image",
        "target_area.name as target_name"
      )
      .from("pose")
      .innerJoin("target_area", "pose.target_area_id", "target_area.id");
    if (search) {
      query.where("pose.name", "like", `%${search}%`);
    }
    if (targetId) {
      query.where("pose.target_area_id", targetId);
    }
    if (levelId) {
      query.where("pose.experience_id", levelId);
    }
    //   if(search&& targetId && levelId){
    //     query
    //     .where("pose.target_area_id", targetId)
    //     .andWhere("pose.experience_id", levelId)
    //     .andWhere("pose.name", 'like', `%${search}%`)
    //   } else if (targetId && levelId) {
    //   query
    //     .where("pose.target_area_id", targetId)
    //     .andWhere("pose.experience_id", levelId);
    // } else if (targetId) {
    //   query.where("pose.target_area_id", targetId);
    // } else if (levelId) {
    //   query.where("pose.experience_id", levelId);
    // }
    return await query;
  }
}
