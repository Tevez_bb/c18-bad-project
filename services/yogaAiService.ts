// // what is needed from the DB
// // By receiving pose.id from the brower, selecting the pose info from the DB
// // Storing the best record into the DB, ( history, user, time: second???)

import { Knex } from "knex";
// import { convertCompilerOptionsFromJson } from "typescript";

export class YogaAiService {
  constructor(private knex: Knex) {}

  // async getPoseInfo(): Promise<any> {
  //   return await console.log("connect to AI Service: Get pose");
  // }

  // async storePosingRecord() {
  //   // get the time from the browser, which is second
  //   //let workedPose_id = 1
  //   //let time = 10
  //   // await this.knex("history")
  //   return await console.log("connect to AI Service: Store record");
  // }

  async bookmark(user_id: number | undefined, pose_id: number | undefined) {
    const hasBookmarked =
      (
        await this.knex.raw(
          `select * from bookmark where user_id = ${user_id} and pose_id = ${pose_id}`
        )
      ).rowCount > 0;
    if (hasBookmarked) {
      await this.knex("bookmark")
        .where("bookmark.user_id", user_id)
        .andWhere("bookmark.pose_id", pose_id)
        .del();
    } else {
      await this.knex("bookmark").insert({ user_id, pose_id });
    }
  }

  async poseDetail(pose_id: number | undefined) {
    return await this.knex
      .select(
        "pose.class_id",
        "pose.name as pose_name",
        "pose.image",
        "target_area.name as target_name"
      )
      .from("pose")
      .innerJoin("target_area", "pose.target_area_id", "target_area.id")
      .where("pose.id", pose_id);
    // console.log(query.toSQL());
    // return await query;
  }

  async postScore(
    completed: boolean,
    user_id: number | undefined,
    pose_id: number | undefined,
    accuracy: number
  ) {
    return await this.knex("history").insert({
      completed,
      user_id,
      pose_id,
      accuracy,
    });
  }

  async checkBookmark(user_id: number | undefined, pose_id: number | undefined) {
    const bookmarked =
      (
        await this.knex.raw(
          `select * from bookmark where user_id = ${user_id} and pose_id = ${pose_id}`
        )
      ).rowCount > 0;

      if (bookmarked){
        console.log(`pose_id${pose_id} has been bookmarked by user${user_id}`)
      }else{
        console.log("bookmark status in service: ", bookmarked)
      }

      return bookmarked

  }
}
