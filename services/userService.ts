import fetch from "node-fetch";
import { hashPassword } from "../util/hash";
import crypto from "crypto";
import { Knex } from "knex";

export class UserService { 
    constructor(private knex: Knex) {} 

    public async findUserById(id: number) {
        let foundUser = await this.knex("user").select("*").where("id", id).first();
        return foundUser;
    }

    public async findUser(email: string) {
        let foundUser = await this.knex("user").select("*").where("email", email).first();
        // let userResult = await this.client.query(`select * from users where username = $1`, [username]);
        // let foundUser = userResult.rows[0];
        // return foundUser;
        return foundUser;
    }

    public async getGoogleInfo(accessToken: string) {
        const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
            method: "get",
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        }); 

        const result = await fetchRes.json();
        return result;
    }

    public async createUser(name: string, password: string, icon: string, email: string) {
        let hashedPasswordString;
        if (!password) {
            hashedPasswordString = await hashPassword(crypto.randomBytes(20).toString("hex"));
        } else {
            hashedPasswordString = await hashPassword(password);
        }
        let googleUserResult =  this.knex("user").insert({name, password:hashedPasswordString, icon, email});
        // await this.client.query("INSERT INTO users (username,password, created_at, updated_at) values ($1,$2, NOW(), NOW())", [username, hashedPasswordString]);
        return await googleUserResult;
    } 
}   
