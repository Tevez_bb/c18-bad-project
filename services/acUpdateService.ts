import { Knex } from "knex";
export class AcUpdateService {
    constructor(private knex: Knex) {}

    async getAcDetail(user_id: number){
        return await this.knex.select(
            "user_target_area.target_area_id"
        ).from("user_target_area")
        .where("user_target_area.user_id", user_id)
        .orderBy("target_area_id", "desc")
    }

    async updateAcDetail(user_id: number, gender:string, experience_id:number, target_area_ids:number[], age_group:string) {
        console.log({user_id, gender, experience_id, age_group, target_area_ids})
        await this.knex("user").update({gender, experience_id, age_group}).where('id', user_id)
        // let user_id = await this.knex("user").returning("id").update({experience_id, age_group})
            // console.log(`user id`, user_id);
        // user_id = user_id[0]['id']

        // console.log(target_area_ids);
        let targetAreaInfos = target_area_ids.map(target_area_id => {
            return { user_id, target_area_id }
        })
        // console.log(targetAreaInfos);
        
        // Delete All record user_target_area by user_id and target_area_id
        // insert targetAreaInfos into user_target_area

        // for (let target_area_id of target_area_ids){
            await this.knex("user_target_area")
            // .where({ user_id, target_area_id})
            .where({ user_id})
            .del()
        // }

        await this.knex("user_target_area")
        .insert(targetAreaInfos)
        .where("user_id", user_id)
        // console.log(targetAreaInfo);
        //     await this.knex("user_target_area")
        //     .update(targetAreaInfo)
        //     .where("user_id", user_id) 
    }
}