import { Request, Response } from "express";
import { CatalogService } from "../services/catalog-service";

export class CatalogController {
  constructor(private catalogService: CatalogService) {}

  getLevel = async (req: Request, res: Response) => {
    let levelResult = await this.catalogService.getAllLevels();
    res.json(levelResult);
  };

  getTarget = async (req: Request, res: Response) => {
    let targetResult = await this.catalogService.getAllTargets();
    res.json(targetResult);
  };

  getCatalog = async (req: Request, res: Response) => {
    // console.log("targetId : ", req.query.targetId);
    let targetId = req.query.targetId
      ? parseInt(req.query.targetId as string)
      : undefined;
    let levelId = req.query.levelId
      ? parseInt(req.query.levelId as string)
      : undefined;
    let search = req.query.search as string;
    // if (targetId != undefined) {
    //   targetId = parseInt(targetId)
    // }
    // const targetId = parseInt(req.query.targetId)
    // let {targetId,levelId}=req.query
    let catalogResult = await this.catalogService.getCatalogItems(
      targetId,
      levelId,
      search
    );
    res.json(catalogResult);
  };
}
