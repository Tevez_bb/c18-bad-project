import { Request, Response } from "express";
import { UserInfoService } from "../services/userInfo-service";

export class UserInfoController {
  constructor(private userInfoService: UserInfoService) {}

  getTry = async (req: Request, res: Response) => {
    let user_id = req.session["user"].id;
    let tryPoseResult = await this.userInfoService.getTryPose(user_id);
    res.json(tryPoseResult);
  };

  getCompleted = async (req: Request, res: Response) => {
    let user_id = req.session["user"].id;
    let completedPoseResult = await this.userInfoService.getCompletedPose(
      user_id
    );
    res.json(completedPoseResult);
  };

  getWish = async (req: Request, res: Response) => {
    let user_id = req.session["user"].id;
    let wishPoseResult = await this.userInfoService.getWishPose(user_id);
    res.json(wishPoseResult);
  };

  getLeader = async (req: Request, res: Response) => {
    let leaderResult = await this.userInfoService.getCompletedNum();
    res.json(leaderResult);
  };
}
