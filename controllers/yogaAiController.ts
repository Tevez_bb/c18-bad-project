import { Request, Response } from "express";
import { YogaAiService } from "../services/yogaAiService";

export class YogaAiController {
  constructor(private yogaAiService: YogaAiService) { }

  toggleBookmark = async (req: Request, res: Response) => {
    let user_id = req.session["user"].id;
    let pose_id = req.query.poseId
      ? parseInt(req.query.poseId as string)
      : undefined;

    console.log("try to book: user_ Pose_", user_id, pose_id)
    let bookmarkResult = await this.yogaAiService.bookmark(user_id, pose_id);
    res.json(bookmarkResult);
    // res.json({ message: "Connect to bookmark function" });
  };

  getPoseDetail = async (req: Request, res: Response) => {
    let pose_id = req.query.poseId
      ? parseInt(req.query.poseId as string)
      : undefined;
    console.log("server get: ", pose_id);
    let poseDetailResult = await this.yogaAiService.poseDetail(pose_id);
    console.log("poseDetailResult: ", poseDetailResult);
    res.json(poseDetailResult);
  };

  postHistory = async (req: Request, res: Response) => {
    try {
      let pose_id = req.query.poseId
        ? parseInt(req.query.poseId as string)
        : undefined;
      let user_id = req.session["user"].id;
      let completed = true;
      let accuracy = req.body.accuracy;
      // console.log("received time: ", accuracy);
      // console.log("received poseID: ", pose_id);
      // console.log("received user_id: ", user_id);
      await this.yogaAiService.postScore(completed, user_id,pose_id, accuracy);
      res.json("record pose completed");
    } catch (error) {
      console.error(error);
    }

  };

  checkBookmark = async (req: Request, res: Response) => {
    let user_id = req.session["user"].id;
    let pose_id = req.query.poseId
      ? parseInt(req.query.poseId as string)
      : undefined;

    console.log("try to check: user_ Pose_", user_id, pose_id)
    let bookmarkResult = await this.yogaAiService.checkBookmark(user_id, pose_id);
    console.log("bk mark res in controller: ",bookmarkResult)
    res.json(bookmarkResult);
    // res.json({ message: "Connect to bookmark function" });

  };
}
