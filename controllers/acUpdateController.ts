import { Request, Response } from "express";
import { AcUpdateService } from "../services/acUpdateService";

export class AcUpdateController {
    constructor(private acUpdateService: AcUpdateService) {}

    getAccountDetail = async (req: Request, res:Response) => {
    
        let user_id = req.session["user"].id;
        console.log(`user_id`, user_id);
        
        let acResult = await this.acUpdateService.getAcDetail(user_id);
        res.json(acResult);
    };

    updateAccountDetail = async (req: Request, res:Response) => {

        const { gender, experience_id, age_group, target_area_ids } = req.body;
        // console.log(req.body);·
        let user_id = req.session["user"]?.id || 41;

        this.acUpdateService.updateAcDetail( user_id, gender, experience_id, target_area_ids, age_group)
        res.json({ message: "Account updated. Redirecting back to profile page."});
    };
}
