import { Request, Response } from "express";
import { RegisterService } from "../services/register-service";
import { UserService } from "../services/userService";
export class RegisterController {
    constructor(private registerService: RegisterService, private userService: UserService) {}

    registerAcc = async (req: Request, res: Response) => {
        const { email, name, password, confirm_password, gender, experience_id, age_group, target_area_id } = req.body;
        
        const file = req.file;
        
        if (!name) {
            res.status(401).json({
                message: "Please fill in name",
                success: false,
            });
            return;
        }
        if (!email) {
            res.status(401).json({
                message: "Please fill in email",
                success: false,
            });
            return;
        }
        if (!password) {
            res.status(401).json({
                message: "Please fill in password",
                success: false,
            });
            return;
        }
        if(confirm_password != password) {
            res.status(401).json({
                message: "Passwords don't match, please re-enter",
                success: false,
            });
            return;
        }

        if(!target_area_id.length) {
            res.status(401).json({
                message: "Please select a target area",
                success: false,
            });
            return;
        }
        // console.log(target_area_id);
        
        let foundEmail = await this.userService.findUser(email);

        if (foundEmail){
            res.status(401).json({
                message: "Email already registered",
                success: false,
            });
            return;
            
        }

        let fileName = file ? file.filename : "";
        // let updatedTargetAreaIds:number[] = target_area_id.map((id:string)=>{
        //     return parseInt(id)
        // })
        const userId = await this.registerService.register(email, name, password, fileName, experience_id, gender, age_group, JSON.parse(target_area_id));
        let foundUser = await this.userService.findUserById(userId);
        req.session["user"] = foundUser;

        // this.userService.findUserById(userId)
        
        // UserService.findUser => return (foundUser)
        res.json({ message: req.session["user"]})
    };
}