import express from "express";
import { UserService } from "../services/userService";
import { Request, Response } from "express";
import { createResultJSON } from "../util/response-helper";
import { checkPassword } from "../util/hash";

export class UserController {
    constructor(private userService: UserService) {}

    public login = async (req: Request, res: Response) => {
        const { email, password } = req.body;
        // console.log(req.body); //password not deleted
        // if (!email || !password) {
        //     res.status(400).json(createResultJSON(null, { message: "Invalid input" }));
        // }
        let foundUser = await this.userService.findUser(email);
        console.log(`foundUser`, foundUser);
        
        if (!foundUser){
            res.status(401).json({
                message: "Email or password is incorrect",
                success: false,
            });
            return 
        }
        let isPwValid = await checkPassword(password, foundUser.password);
        // console.log(isPwValid);
        if (!isPwValid){
            res.status(401).json({
                message: "Password is incorrect",
                success: false,
            });
            return 
        }
        if (isPwValid) {
            delete foundUser.password;        
            req.session["user"] = foundUser;  //
            // console.log(req.session["user"]); //password deleted
            res.json({
                message: "Login successfully",
                success: true,
            });
        } 
    };

    public logout = async (req: Request, res: Response) => {
        delete req.session["user"];
        // req.session.destroy(() => {};
        res.json(createResultJSON("logout success"));
    };

    public getUser = async (req: Request, res: Response) => {
        // console.log(req.session["user"])
        if (req.session && req.session["user"]) {
            res.json(createResultJSON({ user: req.session["user"] }));
            return;
        }
        res.status(400).json(createResultJSON(null, { message: "No login user" }))     
    };

    loginGoogle = async (req: express.Request, res: express.Response) => {

        const accessToken = req.session?.["grant"].response?.access_token;
     
        let googleUserInfo = await this.userService.getGoogleInfo(accessToken);
       
        let foundGoogleUser = await this.userService.findUser(googleUserInfo.email);
    
        if (!foundGoogleUser) {
            await this.userService.createUser(googleUserInfo.name, "1234", googleUserInfo.picture, googleUserInfo.email);
            foundGoogleUser = await this.userService.findUser(googleUserInfo.email);
        }

        if (req.session) {
            req.session["user"] = foundGoogleUser;
        }
        return res.redirect("/");
    };
}
