export interface Level {
  name: string;
  id: number;
}

export interface Target {
  name: string;
}

// export interface CatalogItem {
//   content: string;
//   image: string;
//   id: number;
//   status: string;
// }
