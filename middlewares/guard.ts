// import { NextFunction, Request, Response } from "express";

// export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
//   //1. req.session['user']係咪有內容
//   if (req.session["user"]) {
//     //2. 有內容，就即係login 咗 -> next()，繼續去原本去緊嘅地方
//     next();
//   } else {
//     //3.無內容，即係未login -> 答401
//     res.status(401).json({ msg: "UnAuthorized" });
//   }
// }

import {Request,Response,NextFunction} from 'express';

export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        next();
    }else{
        res.redirect('/login.html');
    }
}

