import grant from "grant";
import env from "../env";
import expressSession from "express-session";
import express from "express";

export const grantExpress = grant.express({
  defaults: {
    origin: env.GOOGLE_ORIGIN,
    transport: "session",
    state: true,
  },
  google: {
    key: env.GOOGLE_CLIENT_ID || "",
    secret: env.GOOGLE_CLIENT_SECRET || "",
    scope: ["profile", "email"],
    callback: "/login/google",
  },
});

export const expressSessionGate = expressSession({
  secret: "Tecky Academy teaches typescript",
  resave: true,
  saveUninitialized: true,
});

export function requestLogger(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  // logger.error(`${req.method}-${req.path}`);
  // logger.warn(`${req.method}-${req.path}`);
  // logger.info(`${req.method}-${req.path}`);
  // logger.debug(`${req.method}-${req.path}`);
  next();
}

export function handle404(req: express.Request, res: express.Response) {
  console.log(`Requesting : ${req.method}-${req.path}  but got 404`);
  res.redirect("/404.html");
}
