Model frame reference: Part 1 and Part 2 ONLY
https://www.tensorflow.org/lite/tutorials/pose_classification#optional_code_snippet_to_try_out_the_movenet_pose_estimation_logic

Model layer modified reference: line 122, and self-preprocessed Input at line 112
https://github.com/harshbhatt7585/YogaIntelliJ/blob/main/classification%20model/training.py


MoveNet Introduction:
https://tfhub.dev/s?q=movenet


Coding reference:
https://ithelp.ithome.com.tw/articles/10225146
https://tf.wiki/zh_hant/deployment/javascript.html

tensorflowJS info and conversion:
https://www.tensorflow.org/js/tutorials/setup


What this model is doing:
Using MoveNet from tensorflow (movenet_thunder.tflite) to process the image and locate the 17 joint keypoints

Keypoints of the image of each class is stored into CSV (testing set, and training set)

Train the model

Convert the model to be TensorflowJS-readable
*Please notice the image preparation from the 1st link~