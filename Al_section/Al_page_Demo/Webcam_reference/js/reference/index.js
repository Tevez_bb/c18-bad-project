

const video = document.getElementById('webcam')
// const liveView = document.getElementById('liveView'); // Container.
const demosSection = document.getElementById('demos') 
const enableWebcamButton = document.getElementById('webcamButton')
const disableWebcamButton = document.getElementById('webcamDisableButton')

//Checking webcam has been successfully imported, so that set model is TRUE
//AND demosSection's Class "invisible" is set to be removed 
let model = true;
demosSection.classList.remove('disabled')


function getUserMediaSupported() {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getDisplayMedia)
}

function enableCam(event) {
    //To check the machine Learning Model has been loaded
    if (!model) { return; }
    event.target.classList.add('removed');
    const constraints = {
        video: true,
        audio: false
    }

    //Introducing the webCam Video-only stream
    navigator.mediaDevices.getUserMedia(constraints).then(async(stream) => {
        video.srcObject = stream;
        disableWebcamButton.classList.remove('unseen')
        disableWebcamButton.addEventListener('click', location.reload);
        // video.addEventListener('loadeddata', predictWebcam)
        const detectorConfig = {
            modelType: poseDetection.movenet.modelType.SINGLEPOSE_LIGHTNING
        };
        const detector = await poseDetection.createDetector(
            poseDetection.SupportedModels.MoveNet,
            detectorConfig
        );
    })
}




//Checking the browser in use is supporting or not
if (getUserMediaSupported()) {
    enableWebcamButton.addEventListener('click', enableCam);
    
} else {
    console.warn(`Fail to get webcam feed: no browser supports`)
}