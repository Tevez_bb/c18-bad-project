// can draw and can classify



const enableWebcamButton = document.getElementById('webcamButton')
const disableWebcamButton = document.getElementById('webcamDisableButton')
const demosSection = document.getElementById('demos')
const video = document.getElementById('webcam')
// flag variable is used to help capture the time when AI just detect 
// the pose as correct(probability more than threshold)
let flag = false;
let skeletonColor = 'rgb(255,255,255)';

const CLASS_NO = {
    Mountain: 0,
    SleepingVishnu: 1,
    Tree: 2,
}

const POINTS = {
    NOSE: 0,
    LEFT_EYE: 1,
    RIGHT_EYE: 2,
    LEFT_EAR: 3,
    RIGHT_EAR: 4,
    LEFT_SHOULDER: 5,
    RIGHT_SHOULDER: 6,
    LEFT_ELBOW: 7,
    RIGHT_ELBOW: 8,
    LEFT_WRIST: 9,
    RIGHT_WRIST: 10,
    LEFT_HIP: 11,
    RIGHT_HIP: 12,
    LEFT_KNEE: 13,
    RIGHT_KNEE: 14,
    LEFT_ANKLE: 15,
    RIGHT_ANKLE: 16,
}

const keypointConnections = {
    nose: ['left_ear', 'right_ear'],
    left_ear: ['left_shoulder'],
    right_ear: ['right_shoulder'],
    left_shoulder: ['right_shoulder', 'left_elbow', 'left_hip'],
    right_shoulder: ['right_elbow', 'right_hip'],
    left_elbow: ['left_wrist'],
    right_elbow: ['right_wrist'],
    left_hip: ['left_knee', 'right_hip'],
    right_hip: ['right_knee'],
    left_knee: ['left_ankle'],
    right_knee: ['right_ankle']
}
function drawSegment(ctx, [mx, my], [tx, ty], color) {
    ctx.beginPath()
    ctx.moveTo(mx, my)
    ctx.lineTo(tx, ty)
    ctx.lineWidth = 5
    ctx.strokeStyle = color
    ctx.stroke()
}

function drawPoint(ctx, x, y, r, color) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.fillStyle = color;
    ctx.fill();
}

console.log("with classify model loaded_color 0.6")
demosSection.classList.remove('disabled')

function getUserMediaSupported() {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getDisplayMedia)
}

{
    // preprocessing
    function get_center_point(landmarks, left_bodypart, right_bodypart) {
        let left = tf.gather(landmarks, left_bodypart, 1)
        let right = tf.gather(landmarks, right_bodypart, 1)
        const center = tf.add(tf.mul(left, 0.5), tf.mul(right, 0.5))
        return center

    }

    function get_pose_size(landmarks, torso_size_multiplier = 2.5) {
        let hips_center = get_center_point(landmarks, POINTS.LEFT_HIP, POINTS.RIGHT_HIP)
        let shoulders_center = get_center_point(landmarks, POINTS.LEFT_SHOULDER, POINTS.RIGHT_SHOULDER)
        let torso_size = tf.norm(tf.sub(shoulders_center, hips_center))
        let pose_center_new = get_center_point(landmarks, POINTS.LEFT_HIP, POINTS.RIGHT_HIP)
        pose_center_new = tf.expandDims(pose_center_new, 1)

        pose_center_new = tf.broadcastTo(pose_center_new,
            [1, 17, 2]
        )
        // return: shape(17,2)
        let d = tf.gather(tf.sub(landmarks, pose_center_new), 0, 0)
        let max_dist = tf.max(tf.norm(d, 'euclidean', 0))

        // normalize scale
        let pose_size = tf.maximum(tf.mul(torso_size, torso_size_multiplier), max_dist)
        return pose_size
    }

    function normalize_pose_landmarks(landmarks) {
        let pose_center = get_center_point(landmarks, POINTS.LEFT_HIP, POINTS.RIGHT_HIP)
        pose_center = tf.expandDims(pose_center, 1)
        pose_center = tf.broadcastTo(pose_center,
            [1, 17, 2]
        )
        landmarks = tf.sub(landmarks, pose_center)

        let pose_size = get_pose_size(landmarks)
        landmarks = tf.div(landmarks, pose_size)
        return landmarks
    }

    function landmarks_to_embedding(landmarks) {
        // normalize landmarks 2D
        landmarks = normalize_pose_landmarks(tf.expandDims(landmarks, 0))
        let embedding = tf.reshape(landmarks, [1, 34])
        return embedding
    }
    /////////////////////////
}

const runMovenet = async () => {
    console.log("button click")
    const modelToUse = poseDetection.SupportedModels.MoveNet;
    const model = await poseDetection.createDetector(modelToUse);
    const poseClassifier = await tf.loadLayersModel("modelV2.json")
    // const poseClassifier = await tf.loadLayersModel('https://models.s3.jp-tok.cloud-object-storage.appdomain.cloud/model.json')
    // const countAudio = new Audio(count)
    // countAudio.loop = true
    let fpsDIY = 5;
    interval = setInterval(() => {
        app(model, poseClassifier)
    }, 1000 / fpsDIY)
}

async function app(model, poseClassifier) {
    // const modelToUse = poseDetection.SupportedModels.MoveNet;
    // const model = await poseDetection.createDetector(modelToUse);
    // const poseClassifier = await tf.loadLayersModel("modelV2.json")

    const webcamElement = document.getElementById('webcam');
    const canvas = document.getElementById('detect_result');
    const context = canvas.getContext('2d');

    const color = ["green", "yellow", "red"]

    let notDetected = 0
    // let round = 0;
    
    let checkingEstimateOn = true;
    let showResult = async function () {
        if (checkingEstimateOn) {
            // console.log("Estimating poses")
            checkingEstimateOn = false
        }


        const result = await model.estimatePoses(webcamElement);
        canvas.width = webcamElement.videoWidth;
        canvas.height = webcamElement.videoHeight;
        context.drawImage(webcamElement, 0, 0);
        context.font = '40px Arial';
        // console.log("MoveNet result: ", result[0])
        if (result.length) {
            try {
                const pose = result[0];
                const keypoints = pose.keypoints;
                let input = keypoints.map((keypoint) => {
                    if (keypoint.score > 0.4) {
                        if (!(keypoint.name === 'left_eye' || keypoint.name === 'right_eye')) {
                            drawPoint(context, keypoint.x, keypoint.y, 8, 'rgb(255,255,255)')
                            let connections = keypointConnections[keypoint.name]
                            try {
                                connections.forEach((connection) => {
                                    let conName = connection.toUpperCase()
                                    drawSegment(context, [keypoint.x, keypoint.y],
                                        [keypoints[POINTS[conName]].x,
                                        keypoints[POINTS[conName]].y]
                                        , skeletonColor)
                                })
                            } catch (err) {

                            }

                        }
                    } else {
                        notDetected += 1
                    }
                    
                    return [keypoint.x, keypoint.y]

                })
                if (notDetected > 4) {
                    skeletonColor = 'rgb(255,255,255)'
                    // return
                }
                console.log("network:", input)
                const processedInput = await landmarks_to_embedding(input);
                const classification = poseClassifier.predict(processedInput);
                // code for checking
                
                classification.array().then((data) => {
                    // let currentPose = 'Tree';
                    let currentPose = 'Tree';
                    // console.log(currentPose)
                    const classNo = CLASS_NO[currentPose]
                    // let classNo = 1;
                    console.log(data[0][classNo]);
                    if(data[0][classNo] > 0.97) {
            
                        if(!flag) {
                        //   countAudio.play()
                        //   setStartingTime(new Date(Date()).getTime())
                          flag = true
                        }
                        // setCurrentTime(new Date(Date()).getTime()) 
                        skeletonColor = 'rgb(0,255,0)'
                      } else {
                        flag = false
                        skeletonColor = 'rgb(255,255,255)'
                        // countAudio.pause()
                        // countAudio.currentTime = 0
                      }
                })

            } catch (error) {

            }

            // const processedInput = landmarks_to_embedding(input);
            // // console.log("network:", input)
            // const classification = poseClassifier.predict(processedInput);
            // classification.array().then((data) => {
            //     console.log(currentPose)
            //     const classNo = CLASS_NO[currentPose]
            //     console.log(data[0][classNo])
            // })




        }

        // setTimeout(function () {
        //     showResult();
        // }, 1000 / fpsDIY);
        // // round += 1
    }

    let setupWebcam = function () {
        return new Promise((resolve, reject) => {
            const navigatorAny = navigator;
            navigator.getUserMedia = navigator.getUserMedia ||
                navigatorAny.webkitGetUserMedia || navigatorAny.mozGetUserMedia ||
                navigatorAny.msGetUserMedia;
            if (navigator.getUserMedia) {
                navigator.getUserMedia({
                    video: true
                },
                    (stream) => {
                        webcamElement.srcObject = stream;
                        webcamElement.addEventListener('loadeddata', () => resolve(),
                            false);
                    },
                    (err) => reject(err));
            } else {
                reject("getUserMedia failed");
            }
        });
    }
    setupWebcam().then(
        () => {
            // console.log("WebCam Operating")
            showResult();
        },
        (err) => {
            console.log(err);
        }
    )
}


// enableWebcamButton.addEventListener('click', runMovenet);
runMovenet()


// setup()

// if (getUserMediaSupported()) {
//     enableWebcamButton.addEventListener('click', enableCamAndModel);

// } else {
//     console.warn(`Fail to get webcam feed: no browser supports`)
// }





// console.log("beta")