let string =`boat_pose
bound_angle
bow_pose
cat_pose
chair_pose
child_pose
cobra
cow_face_pose
downward_facing_dog
eagle_pose
extended_hand_to_big_toe_pose
extended_side_angle_pose
fish_pose
happy_baby
head_to_knee_pose
hero_pose
lotus
low_lunge
noose_pose
plank
puppy_pose_deep_backbend
seat_forward_bend
spinal_twist
standing_forward_bend
tree_pose
two_legged_inverted_staff_pose
warrior_I
warrior_II
warrior_III
wide_angle_seat_forward_bend`

let array_result = string.split("\n");
let obj = {};

for (let i = 0; i < array_result.length; i++){
    obj[`${array_result[i]}`]= i;
}

console.log("Object: ", obj)