import express from 'express';
// import {Request,Response} from 'express';

const app = express();
app.use(express.static('model'))
app.use(express.static('Webcam_reference'))

// app.use(express.static('test_movenet_front'))


// app.get('/',function(req:Request,res:Response){
//     res.end("Hello World");
// })

const PORT = 8017;


app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
})