import { Request, Response } from "express";
import { CatalogController } from "../controllers/catalog-controller";
import { CatalogService } from "../services/catalog-service";
import { knex } from "../util/db";
import { Level } from "../model";

describe("catalog controller", () => {
  let catalogController: CatalogController;
  let catalogService: CatalogService;
  let req: Request;
  let res: Response;
  let level: Level[];

  beforeEach(() => {
    level = [
      { name: "Hi", id: 1 },
      { name: "Hello", id: 2 },
      { name: "HaHa", id: 3 },
    ];
    res = {
      json: jest.fn(() => null),
    } as any as Response;
    catalogService = new CatalogService(knex);
    catalogController = new CatalogController(catalogService);
    jest
      .spyOn(catalogService, "getAllLevels")
      .mockImplementation(async () => level);
  });
  it("should get levels", async () => {
    req = {} as Request;
    await catalogController.getLevel(req, res);
    expect(res.json).toBeCalledWith(level);
  });
});
